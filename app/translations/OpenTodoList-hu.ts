<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation type="unfinished">Ról ről...</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="29"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="36"/>
        <source>A todo and task managing application.</source>
        <translation type="unfinished">Teendő és feladatkezelő alkalmazás.</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="64"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation type="unfinished">Az OpenTodoList a &lt;a href=&apos;app-license&apos;&gt; GNU Általános Nyilvános Licenc &lt;/a&gt; 3. verziója vagy (az Ön választása szerint) bármely későbbi verziója alapján kerül kiadásra.</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="83"/>
        <source>Report an Issue</source>
        <translation type="unfinished">Jelentsen egy problémát</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="91"/>
        <source>Copy System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="112"/>
        <source>Third Party Libraries and Resources</source>
        <translation type="unfinished">Harmadik féltől származó könyvtárak és források</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="149"/>
        <source>Author:</source>
        <translation type="unfinished">Szerző:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="154"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="161"/>
        <source>License:</source>
        <translation type="unfinished">Engedély:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="166"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="172"/>
        <source>Download:</source>
        <translation type="unfinished">Letöltés:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="176"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="21"/>
        <source>Select Account Type</source>
        <translation type="unfinished">Válassza a Fiók típusát</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="56"/>
        <source>Account Type</source>
        <translation type="unfinished">Fiók Típus</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="62"/>
        <source>NextCloud</source>
        <translation type="unfinished">NextCloud</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="65"/>
        <source>ownCloud</source>
        <translation type="unfinished">ownCloud</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="68"/>
        <source>WebDAV</source>
        <translation type="unfinished">WebDAV</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="71"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../Pages/AccountsPage.qml" line="19"/>
        <location filename="../Pages/AccountsPage.qml" line="41"/>
        <source>Accounts</source>
        <translation type="unfinished">Fiókok</translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="185"/>
        <source>Manage your personal data.</source>
        <translation type="unfinished">Kezelje személyes adatait.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="191"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation type="unfinished">Kapcsolja be az érintőképernyők néhány optimalizálását.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="194"/>
        <source>Only run the app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="196"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="201"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="334"/>
        <source>Open</source>
        <translation type="unfinished">Nyisd ki</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="341"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="394"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="187"/>
        <source>Background Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="192"/>
        <source>App continues to sync your data in the background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="196"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../Components/ApplicationShortcuts.qml" line="15"/>
        <source>Ctrl+,</source>
        <translation type="unfinished">Ctrl+,</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../Components/ApplicationToolBar.qml" line="151"/>
        <source>Synchronizing library...</source>
        <translation type="unfinished">A könyvtár szinkronizálása...</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBarActions</name>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="16"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="23"/>
        <source>Rename</source>
        <translation type="unfinished">átnevezés</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="34"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="45"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="56"/>
        <source>Color</source>
        <translation type="unfinished">Szín</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="67"/>
        <source>Add Tag</source>
        <translation type="unfinished">Címke hozzáadása</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="74"/>
        <source>Attach File</source>
        <translation type="unfinished">Fájl csatolása</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="83"/>
        <source>Search</source>
        <translation type="unfinished">Keresés</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="90"/>
        <source>Sort</source>
        <translation type="unfinished">Fajta</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="98"/>
        <source>Set Due Date</source>
        <translation type="unfinished">Állítsa be az esedékesség napját</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="106"/>
        <source>Delete</source>
        <translation type="unfinished">Töröl</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="113"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="120"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../Widgets/Attachments.qml" line="28"/>
        <source>Attach File</source>
        <translation type="unfinished">Fájl csatolása</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="40"/>
        <source>Delete Attachment?</source>
        <translation type="unfinished">Törli a mellékletet?</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="44"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation type="unfinished">Biztosan törli a &lt;strong&gt;%1&lt;/strong&gt; mellékletet? Ez a művelet nem visszavonható.</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="58"/>
        <source>Attachments</source>
        <translation type="unfinished">Mellékletek</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation type="unfinished">Szín</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation type="unfinished">Fehér</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation type="unfinished">Piros</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation type="unfinished">Zöld</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation type="unfinished">Kék</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation type="unfinished">Sárga</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation type="unfinished">Narancs</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation type="unfinished">Halványlila</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation type="unfinished">Rendszer</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation type="unfinished">Könnyű</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation type="unfinished">Sötét</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../Actions/CopyTodo.qml" line="11"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../Actions/CopyTopLevelItem.qml" line="11"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../Windows/DeleteAccountDialog.qml" line="19"/>
        <source>Delete Account?</source>
        <translation type="unfinished">Fiók törlése?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteAccountDialog.qml" line="36"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation type="unfinished">Valóban el akarja távolítani a &lt;strong&gt;% 1 &lt;/strong&gt; fiókot? Ezzel eltávolítja az eszközből az összes fiókhoz tartozó könyvtárat? &lt;br/&gt; &lt;br/&gt; &lt;i&gt; Megjegyzés: A fiók visszaadásával visszaállíthatja azokat a kiszolgálóról. &lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../Actions/DeleteCompletedChildren.qml" line="8"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="19"/>
        <source>Delete Completed Items?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="48"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../Actions/DeleteItem.qml" line="8"/>
        <source>Delete</source>
        <translation type="unfinished">Töröl</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="19"/>
        <source>Delete Item?</source>
        <translation type="unfinished">Törli az elemet?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="32"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Tényleg törli a &lt;strong&gt;%1&lt;/strong&gt; képet? Ezt nem lehet visszacsinálni.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="36"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Tényleg törli a &lt;strong&gt;%1&lt;/strong&gt; teendők listáját? Ezt nem lehet visszacsinálni.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Tényleg törli a &lt;strong&gt;%1&lt;/strong&gt; todo-t? Ezt nem lehet visszacsinálni.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="44"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Valóban törli a &lt;strong&gt;%1&lt;/strong&gt; feladatot? Ezt nem lehet visszacsinálni.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="48"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Valóban törli a &lt;strong&gt;%1&lt;/strong&gt; megjegyzést? Ezt nem lehet visszacsinálni.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="52"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Tényleg törli a &lt;strong&gt;%1&lt;/strong&gt; oldalt? Ezt nem lehet visszacsinálni.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="19"/>
        <source>Delete Library?</source>
        <translation type="unfinished">Törli a könyvtárat?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="33"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation type="unfinished">Valóban el akarja távolítani a &lt;strong&gt;%1 &lt;/strong&gt; könyvtárat az alkalmazásból? &lt;em&gt; Ez eltávolítja a könyvtárhoz tartozó összes fájlt. &lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="40"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation type="unfinished">Valóban el akarja távolítani a &lt;strong&gt;%1&lt;/strong&gt; könyvtárat az alkalmazásból? Vegye figyelembe, hogy a könyvtárban lévő fájlokat nem távolítják el, így később visszaállíthatják a könyvtárat.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="27"/>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="67"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Csatlakozás beállításai</translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="75"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="86"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="93"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="97"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="106"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="112"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="116"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="125"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="132"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="23"/>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="59"/>
        <source>Edit Account</source>
        <translation type="unfinished">Fiók szerkesztése</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="65"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="76"/>
        <source>Server Address:</source>
        <translation type="unfinished">Szerver címe:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="83"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="104"/>
        <source>User:</source>
        <translation type="unfinished">Felhasználó:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="111"/>
        <source>User Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="117"/>
        <source>Password:</source>
        <translation type="unfinished">Jelszó:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="124"/>
        <source>Password</source>
        <translation type="unfinished">Jelszó</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="133"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">A tanúsítvány-ellenőrzések letiltása</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="142"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Nem sikerült kapcsolódni a szerverhez. Ellenőrizze felhasználónevét, jelszavát és a szerver címét, majd próbálja újra.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="33"/>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="118"/>
        <source>Edit Account</source>
        <translation type="unfinished">Fiók szerkesztése</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="124"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="135"/>
        <source>Server Address:</source>
        <translation type="unfinished">Szerver címe:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="141"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="148"/>
        <source>User:</source>
        <translation type="unfinished">Felhasználó:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="154"/>
        <source>User Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="161"/>
        <source>Password:</source>
        <translation type="unfinished">Jelszó:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="168"/>
        <source>Password</source>
        <translation type="unfinished">Jelszó</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="178"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">A tanúsítvány-ellenőrzések letiltása</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="186"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Nem sikerült kapcsolódni a szerverhez. Ellenőrizze felhasználónevét, jelszavát és a szerver címét, majd próbálja újra.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="66"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation type="unfinished">&lt;strong&gt;%1 &lt;/strong&gt; létrehozva.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="72"/>
        <source>Open</source>
        <translation type="unfinished">Nyisd ki</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="77"/>
        <source>Dismiss</source>
        <translation type="unfinished">elvetése</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="28"/>
        <source>Due on</source>
        <translation type="unfinished">Esedékes</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="59"/>
        <source>First due on %1.</source>
        <translation type="unfinished">Először a (z) %1 en esedékes.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="71"/>
        <source>No recurrence pattern set...</source>
        <translation type="unfinished">Nincs beállítva visszatérési mintázat...</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="73"/>
        <source>Recurs every day.</source>
        <translation type="unfinished">Minden nap visszatér.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="75"/>
        <source>Recurs every week.</source>
        <translation type="unfinished">Minden héten megismétlődik.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="77"/>
        <source>Recurs every month.</source>
        <translation type="unfinished">Minden hónapban ismétlődik.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="79"/>
        <source>Recurs every year.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="81"/>
        <source>Recurs every %1 days.</source>
        <translation type="unfinished">Minden %1 naponként megismétlődik.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="83"/>
        <source>Recurs every %1 weeks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="85"/>
        <source>Recurs every %1 months.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="32"/>
        <source>Notes</source>
        <translation type="unfinished">Megjegyzések</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="86"/>
        <source>No notes added yet - click here to add some.</source>
        <translation type="unfinished">Még nincs jegyzet hozzáadva - kattintson ide ide, ha hozzá szeretne adni.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="115"/>
        <source>Export to File...</source>
        <translation type="unfinished">Exportálás a fájlba ...</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="119"/>
        <source>Markdown files</source>
        <translation type="unfinished">Markdown fájlok</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="120"/>
        <source>All files</source>
        <translation type="unfinished">Minden fájl</translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="152"/>
        <source>Move Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="167"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="184"/>
        <source>Copy Item Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="202"/>
        <source>Copy Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="187"/>
        <source>New Library</source>
        <translation type="unfinished">Új könyvtár</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="193"/>
        <source>Accounts</source>
        <translation type="unfinished">Fiókok</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="200"/>
        <source>Edit List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="206"/>
        <source>Settings</source>
        <translation type="unfinished">Beállítások</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="213"/>
        <source>Translate The App...</source>
        <translation type="unfinished">Az alkalmazás lefordítása...</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="220"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="228"/>
        <source>About...</source>
        <translation type="unfinished">Ról ről...</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="241"/>
        <source>Create Default Library</source>
        <translation type="unfinished">Alapértelmezett könyvtár létrehozása</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="353"/>
        <source>Hide Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="353"/>
        <source>Show Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="364"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="373"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="386"/>
        <source>Schedule</source>
        <translation type="unfinished">Menetrend</translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="161"/>
        <source>Red</source>
        <translation type="unfinished">Piros</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="168"/>
        <source>Green</source>
        <translation type="unfinished">Zöld</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="175"/>
        <source>Blue</source>
        <translation type="unfinished">Kék</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="182"/>
        <source>Yellow</source>
        <translation type="unfinished">Sárga</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="189"/>
        <source>Orange</source>
        <translation type="unfinished">Narancs</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="196"/>
        <source>Lilac</source>
        <translation type="unfinished">Halványlila</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="203"/>
        <source>White</source>
        <translation type="unfinished">Fehér</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="213"/>
        <source>Rename</source>
        <translation type="unfinished">átnevezés</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="218"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="223"/>
        <source>Delete</source>
        <translation type="unfinished">Töröl</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="232"/>
        <source>Select Image</source>
        <translation type="unfinished">Válassza a Kép lehetőséget</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="291"/>
        <source>Note Title</source>
        <translation type="unfinished">Megjegyzés címe</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="304"/>
        <source>Todo List Title</source>
        <translation type="unfinished">Todo lista címe</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="317"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="396"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation type="unfinished">Még semmi itt! Kezdje hozzá egy &lt;a href=&apos;#note&apos;&gt; megjegyzés &lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt; todo list &lt;/a&gt; vagy &lt;a href=&apos;#image&apos;&gt; kép &lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="456"/>
        <source>Sort By</source>
        <translation type="unfinished">Sorrend</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="461"/>
        <source>Manually</source>
        <translation type="unfinished">Manuálisan</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="468"/>
        <source>Title</source>
        <translation type="unfinished">Cím</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="475"/>
        <source>Due To</source>
        <translation type="unfinished">Következtében</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="482"/>
        <source>Created At</source>
        <translation type="unfinished">Létrehozva</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="489"/>
        <source>Updated At</source>
        <translation type="unfinished">Frissítve</translation>
    </message>
</context>
<context>
    <name>LibraryPageActions</name>
    <message>
        <location filename="../Menues/LibraryPageActions.qml" line="30"/>
        <source>Sync Now</source>
        <translation type="unfinished">Szinkronizálás most</translation>
    </message>
    <message>
        <location filename="../Menues/LibraryPageActions.qml" line="41"/>
        <source>Sync Log</source>
        <translation type="unfinished">Szinkronizálási napló</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="14"/>
        <source>Synchronization Log</source>
        <translation type="unfinished">Szinkronizálási napló</translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="26"/>
        <source>Copy Log</source>
        <translation type="unfinished">Napló másolása</translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="32"/>
        <source>Scroll to Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="38"/>
        <source>Scroll to Bottom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Windows/MainWindow.qml" line="24"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="276"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation type="unfinished">Kezdje &lt;a href=&apos;#newLibrary&apos;&gt; új könyvtár létrehozásával &lt;/a&gt;. A könyvtárakat különféle elemek tárolására használják, például jegyzeteket, teendőlistákat és képeket.</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../Actions/MoveTodo.qml" line="11"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="54"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Csatlakozás beállításai</translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="62"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="73"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="80"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="86"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="95"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="101"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="105"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="114"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="121"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="92"/>
        <source>Today</source>
        <translation type="unfinished">Ma</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="93"/>
        <source>Tomorrow</source>
        <translation type="unfinished">Holnap</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="94"/>
        <source>This Week</source>
        <translation type="unfinished">Ezen a héten</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="95"/>
        <source>Next Week</source>
        <translation type="unfinished">Jövő héten</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="96"/>
        <source>Select...</source>
        <translation type="unfinished">Kiválasztás ...</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="115"/>
        <source>Title:</source>
        <translation type="unfinished">Cím:</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="120"/>
        <source>The title for your new item...</source>
        <translation type="unfinished">Az új tétel címe ...</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="126"/>
        <source>Create in:</source>
        <translation type="unfinished">Létrehozás:</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="166"/>
        <source>Due on:</source>
        <translation type="unfinished">Esedékes:</translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="82"/>
        <source>Create Library in Account</source>
        <translation type="unfinished">Hozzon létre könyvtárat a fiókban</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="91"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation type="unfinished">Egy fiókban létrehozott könyvtár szinkronizálva van vele. Ez lehetővé teszi a könyvtár könnyű biztonsági mentését egy kiszolgálóra, majd későbbi onnan történő visszaállítását. Ezenkívül az ilyen könyvtárak megoszthatók más felhasználókkal (ha a szerver ezt megengedi).</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="99"/>
        <source>Existing Libraries</source>
        <translation type="unfinished">Meglévő könyvtárak</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="107"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation type="unfinished">Válasszon egy meglévő könyvtárat a kiszolgálón, hogy hozzáadja azt az alkalmazáshoz.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="113"/>
        <source>No libraries were found on the server.</source>
        <translation type="unfinished">Nem található könyvtár a kiszolgálón.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="122"/>
        <source>Searching existing libraries...</source>
        <translation type="unfinished">Meglévő könyvtárak keresése...</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="165"/>
        <source>Create a New Library</source>
        <translation type="unfinished">Hozzon létre egy új könyvtárat</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="173"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation type="unfinished">Hozzon létre egy új könyvtárat, amelyet szinkronizál a szerverrel. Egy ilyen könyvtár hozzáadható az alkalmazáshoz más eszközökön is, az adatok szinkronizálása céljából.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="187"/>
        <source>My new library&apos;s name</source>
        <translation type="unfinished">Az új könyvtárom neve</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation type="unfinished">Válasszon mappát</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="101"/>
        <source>Open a Folder as a Library</source>
        <translation type="unfinished">Nyisson meg egy mappát könyvtárként</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="109"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation type="unfinished">Bármely mappát használhat könyvtár helyként. &lt;br/&gt;&lt;br/&gt; Ez különösen akkor hasznos, ha másik eszközt (például egy felhőszolgáltató szinkronizálási kliensét) szeretne használni az adatok szinkronizálásához.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="113"/>
        <source>Folder:</source>
        <translation type="unfinished">Folder:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="118"/>
        <source>Path to a folder to use as a library</source>
        <translation type="unfinished">Út egy könyvtárhoz használni kívánt mappához</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="124"/>
        <source>Select</source>
        <translation type="unfinished">Választ</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="129"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="134"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Saját helyi könyvtár neve</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="23"/>
        <location filename="../Pages/NewLibraryPage.qml" line="63"/>
        <source>Create Library</source>
        <translation type="unfinished">Könyvtár létrehozása</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="70"/>
        <source>Local Library</source>
        <translation type="unfinished">Helyi könyvtár</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="77"/>
        <source>Use Folder as Library</source>
        <translation type="unfinished">Használja a Mappát könyvtárként</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="89"/>
        <source>Add Libraries From Your Accounts</source>
        <translation type="unfinished">Adjon hozzá könyvtárakat a fiókjából</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="116"/>
        <source>Add Account</source>
        <translation type="unfinished">Fiók hozzáadása</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="62"/>
        <source>Create a Local Library</source>
        <translation type="unfinished">Hozzon létre egy helyi könyvtárat</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="70"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation type="unfinished">A helyi könyvtárat kizárólag az Ön készüléke tárolja - ez tökéletesvé teszi az adatvédelem tömörítését! &lt;br/&gt; &lt;br/&gt; Használja azt, ha csak helyi információt szeretne tárolni, és minden adatáról más mechanizmusok segítségével rendszeresen biztonsági másolatot készít. Ha több eszközön keresztül kell elérnie adatait, hozzon létre egy könyvtárat, amelyet ehelyett szinkronizál.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="80"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="85"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Saját helyi könyvtár neve</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="20"/>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="73"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Csatlakozás beállításai</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="79"/>
        <source>Server Address:</source>
        <translation type="unfinished">Szerver címe:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="93"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="109"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="120"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="128"/>
        <source>Log in Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="134"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="143"/>
        <source>Create App Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="152"/>
        <source>Account Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="162"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="168"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="177"/>
        <source>User:</source>
        <translation type="unfinished">Felhasználó:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="184"/>
        <source>User Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="191"/>
        <source>Password:</source>
        <translation type="unfinished">Jelszó:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="198"/>
        <source>Password</source>
        <translation type="unfinished">Jelszó</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="208"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">A tanúsítvány-ellenőrzések letiltása</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="212"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="225"/>
        <source>Account Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="235"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Nem sikerült kapcsolódni a szerverhez. Ellenőrizze felhasználónevét, jelszavát és a szerver címét, majd próbálja újra.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="43"/>
        <source>Todo List</source>
        <translation type="unfinished">Feladatlista</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="48"/>
        <source>Todo</source>
        <translation type="unfinished">Todo</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="54"/>
        <source>Note</source>
        <translation type="unfinished">Jegyzet</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="58"/>
        <source>Image</source>
        <translation type="unfinished">Kép</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="29"/>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="101"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Csatlakozás beállításai</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="107"/>
        <source>Server Address:</source>
        <translation type="unfinished">Szerver címe:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="113"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="119"/>
        <source>User:</source>
        <translation type="unfinished">Felhasználó:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="125"/>
        <source>User Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="131"/>
        <source>Password:</source>
        <translation type="unfinished">Jelszó:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="137"/>
        <source>Password</source>
        <translation type="unfinished">Jelszó</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="146"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">A tanúsítvány-ellenőrzések letiltása</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="150"/>
        <source>Name:</source>
        <translation type="unfinished">Név:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="163"/>
        <source>Account Name</source>
        <translation type="unfinished">Felhasználónév</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="173"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Nem sikerült kapcsolódni a szerverhez. Ellenőrizze felhasználónevét, jelszavát és a szerver címét, majd próbálja újra.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../Widgets/NoteItem.qml" line="75"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../Pages/NotePage.qml" line="132"/>
        <source>Main Page</source>
        <translation type="unfinished">Főoldal</translation>
    </message>
    <message>
        <location filename="../Pages/NotePage.qml" line="195"/>
        <source>New Page</source>
        <translation type="unfinished">Új oldal</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation type="unfinished">Rendszernyelv</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="18"/>
        <location filename="../Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation type="unfinished">Felismert problémák</translation>
    </message>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation type="unfinished">Hiányoznak a fiók hozzáférési adatai</translation>
    </message>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../Actions/PromoteTask.qml" line="12"/>
        <source>Promote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="280"/>
        <location filename="../appstartup.cpp" line="287"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="19"/>
        <source>Quick Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="46"/>
        <source>Quick Notes Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="57"/>
        <source>Open the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="70"/>
        <source>Quick Note Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="83"/>
        <source>Type your notes here...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="133"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="140"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="158"/>
        <source>Save as Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="161"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="169"/>
        <source>Save as Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="171"/>
        <source>Quick Todo List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="30"/>
        <source>Edit Recurrence</source>
        <translation type="unfinished">Ismétlés szerkesztése</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation type="unfinished">Soha</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="44"/>
        <source>Daily</source>
        <translation type="unfinished">Napi</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="45"/>
        <source>Weekly</source>
        <translation type="unfinished">Heti</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="46"/>
        <source>Monthly</source>
        <translation type="unfinished">Havi</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="47"/>
        <source>Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="48"/>
        <source>Every N Days</source>
        <translation type="unfinished">Minden N nap</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="49"/>
        <source>Every N Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="50"/>
        <source>Every N Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="75"/>
        <source>Recurs:</source>
        <translation type="unfinished">visszatér</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="88"/>
        <source>Number of days:</source>
        <translation type="unfinished">Napok száma:</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="106"/>
        <source>Recur relative to the date when marking as done</source>
        <translation type="unfinished">Ismételje meg a dátumhoz képest, amikor megjelölés befejeződött</translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../Actions/RenameItem.qml" line="8"/>
        <source>Rename</source>
        <translation type="unfinished">átnevezés</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="22"/>
        <source>Rename Item</source>
        <translation type="unfinished">Elem átnevezése</translation>
    </message>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="33"/>
        <source>Enter item title...</source>
        <translation type="unfinished">Adja meg az elem címét...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="20"/>
        <source>Rename Library</source>
        <translation type="unfinished">Nevezze át a könyvtárat</translation>
    </message>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="37"/>
        <source>Enter library title...</source>
        <translation type="unfinished">Írja be a könyvtár címét...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="101"/>
        <source>Overdue</source>
        <translation type="unfinished">Lejárt</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="123"/>
        <source>Today</source>
        <translation type="unfinished">Ma</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="124"/>
        <source>Tomorrow</source>
        <translation type="unfinished">Holnap</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="166"/>
        <source>Later This Week</source>
        <translation type="unfinished">Később ezen a héten</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="168"/>
        <source>Next Week</source>
        <translation type="unfinished">Jövő héten</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="169"/>
        <source>Coming Next</source>
        <translation type="unfinished">Jövőben</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="196"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation type="unfinished">Nincs ütemezve ... Adjon meg egy határidőt az elemekhez, hogy itt megjelenjenek.</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../Windows/SelectLibraryDialog.qml" line="22"/>
        <source>Select Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../Windows/SelectTodoListDialog.qml" line="25"/>
        <source>Select Todo List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../Windows/SelectTopLevelItemDialog.qml" line="22"/>
        <source>Select Item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation type="unfinished">Lejárt ezen a héten</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation type="unfinished">A következő héten esedékes</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../Actions/SetDueTo.qml" line="8"/>
        <source>Select Due Date</source>
        <translation type="unfinished">Válassza az Esedékesség dátumot</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation type="unfinished">Ma esedékes</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation type="unfinished">Holnap esedékes</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="45"/>
        <source>Settings</source>
        <translation type="unfinished">Beállítások</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="51"/>
        <source>User Interface</source>
        <translation type="unfinished">Felhasználói felület</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="59"/>
        <source>Language:</source>
        <translation type="unfinished">Nyelv:</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="105"/>
        <source>Theme:</source>
        <translation type="unfinished">Téma:</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="129"/>
        <source>System Tray:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="135"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="141"/>
        <source>Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="158"/>
        <source>Use custom font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="167"/>
        <source>Desktop Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="176"/>
        <source>Use Compact Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="185"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="193"/>
        <source>Use compact todo lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="203"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="210"/>
        <source>Override Scaling Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="217"/>
        <source>Scale Factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="235"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="244"/>
        <source>Library Item Size:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="43"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation type="unfinished">Hiba történt a könyvtár szinkronizálásakor. Kérjük, ellenőrizze, hogy a könyvtár beállításai naprakészek-e.</translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="49"/>
        <source>Ignore</source>
        <translation type="unfinished">Figyelmen kívül hagyni</translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="53"/>
        <source>View</source>
        <translation type="unfinished">Kilátás</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../Widgets/TagsEditor.qml" line="34"/>
        <source>Add Tag</source>
        <translation type="unfinished">Címke hozzáadása</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../Widgets/TodoListItem.qml" line="76"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/TodoListItem.qml" line="128"/>
        <source>✔ No open todos - everything done</source>
        <translation type="unfinished">✔ Nincs nyitott todos - minden kész</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="160"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="172"/>
        <source>Manually</source>
        <translation type="unfinished">Manuálisan</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="178"/>
        <source>Name</source>
        <translation type="unfinished">Név</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="184"/>
        <source>Due Date</source>
        <translation type="unfinished">Esedékesség dátuma</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="190"/>
        <source>Created At</source>
        <translation type="unfinished">Létrehozva</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="196"/>
        <source>Updated At</source>
        <translation type="unfinished">Frissítve</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="209"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="216"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="246"/>
        <source>Todos</source>
        <translation type="unfinished">Todos</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="266"/>
        <source>Add new todo...</source>
        <translation type="unfinished">Új teendő hozzáadása...</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../Pages/TodoPage.qml" line="147"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="172"/>
        <source>Tasks</source>
        <translation type="unfinished">Feladatok</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="178"/>
        <source>Add new task...</source>
        <translation type="unfinished">Új feladat hozzáadása ..</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="274"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="281"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../Widgets/TodosWidget.qml" line="203"/>
        <source>Due on: %1</source>
        <translation type="unfinished">Esedékes: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../Widgets/TodosWidgetDelegate.qml" line="276"/>
        <source>Swipe to mark undone</source>
        <translation type="unfinished">Húzza az visszavonás jelölésére</translation>
    </message>
    <message>
        <location filename="../Widgets/TodosWidgetDelegate.qml" line="278"/>
        <source>Swipe to mark done</source>
        <translation type="unfinished">Húzza az ujját a kész jelölésére</translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation type="unfinished">Elérhető az OpenTodoList % 1 frissítése.</translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation type="unfinished">Figyelmen kívül hagyni</translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation type="unfinished">Letöltés</translation>
    </message>
</context>
</TS>
