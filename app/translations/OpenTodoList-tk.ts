<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tk">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../Pages/AboutPage.qml" line="15"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="29"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="36"/>
        <source>A todo and task managing application.</source>
        <translation type="unfinished">Todo we meseläni dolandyrmak programmasy.</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="64"/>
        <source>OpenTodoList is released under the terms of the &lt;a href=&apos;app-license&apos;&gt;GNU General Public License&lt;/a&gt; version 3 or (at your choice) any later version.</source>
        <translation type="unfinished">OpenTodoList &lt;a href=&apos;app-license&apos;&gt; GNU General Public License &lt;/a&gt; 3-nji wersiýasy ýa-da soňraky wersiýasy boýunça goýberilýär.</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="83"/>
        <source>Report an Issue</source>
        <translation type="unfinished">Bir mesele barada habar beriň</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="91"/>
        <source>Copy System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="112"/>
        <source>Third Party Libraries and Resources</source>
        <translation type="unfinished">Üçünji tarapyň kitaphanalary we çeşmeleri</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="149"/>
        <source>Author:</source>
        <translation type="unfinished">Awtor:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="154"/>
        <source>&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%2&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="161"/>
        <source>License:</source>
        <translation type="unfinished">Ygtyýarnama:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="166"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%1&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="172"/>
        <source>Download:</source>
        <translation type="unfinished">Göçürip al:</translation>
    </message>
    <message>
        <location filename="../Pages/AboutPage.qml" line="176"/>
        <source>&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished">&lt;a href=&apos;%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>AccountTypeSelectionPage</name>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="21"/>
        <source>Select Account Type</source>
        <translation type="unfinished">Hasap görnüşini saýlaň</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="56"/>
        <source>Account Type</source>
        <translation type="unfinished">Hasap görnüşi</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="62"/>
        <source>NextCloud</source>
        <translation type="unfinished">NextCloud</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="65"/>
        <source>ownCloud</source>
        <translation type="unfinished">ownCloud</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="68"/>
        <source>WebDAV</source>
        <translation type="unfinished">WebDAV</translation>
    </message>
    <message>
        <location filename="../Pages/AccountTypeSelectionPage.qml" line="71"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsPage</name>
    <message>
        <location filename="../Pages/AccountsPage.qml" line="19"/>
        <location filename="../Pages/AccountsPage.qml" line="41"/>
        <source>Accounts</source>
        <translation type="unfinished">Hasaplar</translation>
    </message>
</context>
<context>
    <name>AppStartup</name>
    <message>
        <location filename="../appstartup.cpp" line="185"/>
        <source>Manage your personal data.</source>
        <translation type="unfinished">Şahsy maglumatlaryňyzy dolandyryň.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="191"/>
        <source>Switch on some optimizations for touchscreens.</source>
        <translation type="unfinished">Duýgur ekranlar üçin käbir optimizasiýalary açyň.</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="194"/>
        <source>Only run the app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="196"/>
        <source>Only run the app GUI and connect to an existing app background service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="201"/>
        <source>Enable a console on Windows to gather debug output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="334"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="341"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appstartup.cpp" line="394"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../lib/application.cpp" line="187"/>
        <source>Background Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="192"/>
        <source>App continues to sync your data in the background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../lib/application.cpp" line="196"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationShortcuts</name>
    <message>
        <location filename="../Components/ApplicationShortcuts.qml" line="15"/>
        <source>Ctrl+,</source>
        <translation type="unfinished">Ctrl +</translation>
    </message>
</context>
<context>
    <name>ApplicationToolBar</name>
    <message>
        <location filename="../Components/ApplicationToolBar.qml" line="151"/>
        <source>Synchronizing library...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationToolBarActions</name>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="16"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="23"/>
        <source>Rename</source>
        <translation type="unfinished">Adyny üýtgetmek</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="34"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="45"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="56"/>
        <source>Color</source>
        <translation type="unfinished">Reňk</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="67"/>
        <source>Add Tag</source>
        <translation type="unfinished">Bellik goşuň</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="74"/>
        <source>Attach File</source>
        <translation type="unfinished">Faýl dakyň</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="83"/>
        <source>Search</source>
        <translation type="unfinished">Gözlemek</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="90"/>
        <source>Sort</source>
        <translation type="unfinished">Sort</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="98"/>
        <source>Set Due Date</source>
        <translation type="unfinished">Berlen senäni belläň</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="106"/>
        <source>Delete</source>
        <translation type="unfinished">Öçür</translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="113"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Components/ApplicationToolBarActions.qml" line="120"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Attachments</name>
    <message>
        <location filename="../Widgets/Attachments.qml" line="28"/>
        <source>Attach File</source>
        <translation type="unfinished">Faýl dakyň</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="40"/>
        <source>Delete Attachment?</source>
        <translation type="unfinished">Goşundyny pozuň?</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="44"/>
        <source>Are you sure you want to delete the attachment &lt;strong&gt;%1&lt;/strong&gt;? This action cannot be undone.</source>
        <translation type="unfinished">Goşundyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýändigiňize ynanýarsyňyzmy? Bu hereketi yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Widgets/Attachments.qml" line="58"/>
        <source>Attachments</source>
        <translation type="unfinished">Goşundylar</translation>
    </message>
</context>
<context>
    <name>ColorMenu</name>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="15"/>
        <source>Color</source>
        <translation type="unfinished">Reňk</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="19"/>
        <source>White</source>
        <translation type="unfinished">Ak</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="26"/>
        <source>Red</source>
        <translation type="unfinished">Gyzyl</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="33"/>
        <source>Green</source>
        <translation type="unfinished">Greenaşyl</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="40"/>
        <source>Blue</source>
        <translation type="unfinished">Gök</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="47"/>
        <source>Yellow</source>
        <translation type="unfinished">Sary</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="54"/>
        <source>Orange</source>
        <translation type="unfinished">Mämişi</translation>
    </message>
    <message>
        <location filename="../Menues/ColorMenu.qml" line="61"/>
        <source>Lilac</source>
        <translation type="unfinished">Kirpik</translation>
    </message>
</context>
<context>
    <name>Colors</name>
    <message>
        <location filename="../Utils/Colors.qml" line="15"/>
        <source>System</source>
        <translation type="unfinished">Ulgam</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="16"/>
        <source>Light</source>
        <translation type="unfinished">Lightagtylyk</translation>
    </message>
    <message>
        <location filename="../Utils/Colors.qml" line="17"/>
        <source>Dark</source>
        <translation type="unfinished">Garaňky</translation>
    </message>
</context>
<context>
    <name>CopyItemQuery</name>
    <message>
        <location filename="../../lib/datastorage/copyitemquery.cpp" line="120"/>
        <source>Copy of</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTodo</name>
    <message>
        <location filename="../Actions/CopyTodo.qml" line="11"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyTopLevelItem</name>
    <message>
        <location filename="../Actions/CopyTopLevelItem.qml" line="11"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteAccountDialog</name>
    <message>
        <location filename="../Windows/DeleteAccountDialog.qml" line="19"/>
        <source>Delete Account?</source>
        <translation type="unfinished">Hasaby pozuň?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteAccountDialog.qml" line="36"/>
        <source>Do you really want to remove the account &lt;strong&gt;%1&lt;/strong&gt;? This will remove all libraries belonging to the account from your device?&lt;br/&gt;&lt;br/&gt;&lt;i&gt;Note: You can restore them from the server by adding back the account.&lt;/i&gt;</source>
        <translation type="unfinished">Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; hasaby aýyrmak isleýärsiňizmi? Bu hasabyňyza degişli ähli kitaphanalary enjamyňyzdan aýyrar? &lt;br/&gt; &lt;br/&gt; &lt;i&gt; Bellik: Hasaby yzyna goşup serwerden dikeldip bilersiňiz. &lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>DeleteCompletedChildren</name>
    <message>
        <location filename="../Actions/DeleteCompletedChildren.qml" line="8"/>
        <source>Delete Completed Items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteCompletedItemsDialog</name>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="19"/>
        <source>Delete Completed Items?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="42"/>
        <source>Do you really want to delete all done todos in the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/DeleteCompletedItemsDialog.qml" line="48"/>
        <source>Do you really want to delete all done tasks in the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeleteItem</name>
    <message>
        <location filename="../Actions/DeleteItem.qml" line="8"/>
        <source>Delete</source>
        <translation type="unfinished">Öçür</translation>
    </message>
</context>
<context>
    <name>DeleteItemDialog</name>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="19"/>
        <source>Delete Item?</source>
        <translation type="unfinished">Öçürmek?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="32"/>
        <source>Do you really want to delete the image &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Goşundyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýändigiňize ynanýarsyňyzmy? Bu hereketi yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="36"/>
        <source>Do you really want to delete the todo list &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Todo sanawyny &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="40"/>
        <source>Do you really want to delete the todo &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Hakykatdanam todo &lt;strong&gt;%1&lt;/strong&gt; pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="44"/>
        <source>Do you really want to delete the task &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; meseläni pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="48"/>
        <source>Do you really want to delete the note &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Hakykatdanam &lt;strong&gt;%1&lt;/strong&gt; belligi pozmak isleýärsiňizmi? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteItemDialog.qml" line="52"/>
        <source>Do you really want to delete the page &lt;strong&gt;%1&lt;/strong&gt;? This cannot be undone.</source>
        <translation type="unfinished">Sahypany hakykatdanam pozmak isleýärsiňizmi? &lt;strong&gt;%1&lt;/strong&gt;? Muny yzyna gaýtaryp bolmaz.</translation>
    </message>
</context>
<context>
    <name>DeleteLibraryDialog</name>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="19"/>
        <source>Delete Library?</source>
        <translation type="unfinished">Kitaphanany pozuň?</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="33"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from  the application? &lt;em&gt;This will remove any files belonging to the library.&lt;/em&gt;</source>
        <translation type="unfinished">Hitaphanany &lt;strong&gt;%1&lt;/strong&gt; programmadan aýyrmak isleýärsiňizmi? &lt;em&gt; Bu kitaphana degişli islendik faýly aýyrar. &lt;/em&gt;</translation>
    </message>
    <message>
        <location filename="../Windows/DeleteLibraryDialog.qml" line="40"/>
        <source>Do you really want to remove the library &lt;strong&gt;%1&lt;/strong&gt; from the application? Note that the files inside the library will not be removed, so you can restore the library later on.</source>
        <translation type="unfinished">Kitaphanany &lt;strong&gt;%1&lt;/strong&gt; programmadan aýyrmak isleýärsiňizmi? Kitaphananyň içindäki faýllaryň aýrylmajakdygyna üns beriň, soň bolsa kitaphanany dikeldip bilersiňiz.</translation>
    </message>
</context>
<context>
    <name>EditDropboxAccountPage</name>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="27"/>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="67"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="75"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="86"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="93"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="97"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="106"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="112"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="116"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="125"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/EditDropboxAccountPage.qml" line="132"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditNextCloudAccountPage</name>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="23"/>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="59"/>
        <source>Edit Account</source>
        <translation type="unfinished">Hasaby redaktirläň</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="65"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="76"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="83"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="91"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="104"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="111"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="117"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="124"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="133"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../Pages/EditNextCloudAccountPage.qml" line="142"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>EditWebDAVAccountPage</name>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="33"/>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="118"/>
        <source>Edit Account</source>
        <translation type="unfinished">Hasaby redaktirläň</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="124"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="135"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="141"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="148"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="154"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="161"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="168"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="178"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../Pages/EditWebDAVAccountPage.qml" line="186"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>ItemCreatedNotification</name>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="66"/>
        <source>&lt;strong&gt;%1&lt;/strong&gt; has been created.</source>
        <translation type="unfinished">&lt;strong&gt;%1&lt;/strong&gt; döredildi.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="72"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemCreatedNotification.qml" line="77"/>
        <source>Dismiss</source>
        <translation type="unfinished">Işden aýyrmak</translation>
    </message>
</context>
<context>
    <name>ItemDueDateEditor</name>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="28"/>
        <source>Due on</source>
        <translation type="unfinished">Berilmeli</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="59"/>
        <source>First due on %1.</source>
        <translation type="unfinished">Ilki bilen %1 tölemeli.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="71"/>
        <source>No recurrence pattern set...</source>
        <translation type="unfinished">Gaýtalanma nusgasy ýok ...</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="73"/>
        <source>Recurs every day.</source>
        <translation type="unfinished">Her gün gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="75"/>
        <source>Recurs every week.</source>
        <translation type="unfinished">Her hepde gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="77"/>
        <source>Recurs every month.</source>
        <translation type="unfinished">Her aý gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="79"/>
        <source>Recurs every year.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="81"/>
        <source>Recurs every %1 days.</source>
        <translation type="unfinished">Her %1 günden gaýtalanýar.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="83"/>
        <source>Recurs every %1 weeks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/ItemDueDateEditor.qml" line="85"/>
        <source>Recurs every %1 months.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemNotesEditor</name>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="32"/>
        <source>Notes</source>
        <translation type="unfinished">Bellikler</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="86"/>
        <source>No notes added yet - click here to add some.</source>
        <translation type="unfinished">Entek bellikler goşulmady - käbirlerini goşmak üçin şu ýere basyň.</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="115"/>
        <source>Export to File...</source>
        <translation type="unfinished">Faýla eksport ...</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="119"/>
        <source>Markdown files</source>
        <translation type="unfinished">Markdown faýllary</translation>
    </message>
    <message>
        <location filename="../Widgets/ItemNotesEditor.qml" line="120"/>
        <source>All files</source>
        <translation type="unfinished">Fileshli faýllar</translation>
    </message>
</context>
<context>
    <name>ItemUtils</name>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="152"/>
        <source>Move Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="167"/>
        <source>Convert Task to Todo and Move Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="184"/>
        <source>Copy Item Into...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Utils/ItemUtils.qml" line="202"/>
        <source>Copy Todo Into...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibrariesSideBar</name>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="187"/>
        <source>New Library</source>
        <translation type="unfinished">Täze kitaphana</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="193"/>
        <source>Accounts</source>
        <translation type="unfinished">Hasaplar</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="200"/>
        <source>Edit List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="206"/>
        <source>Settings</source>
        <translation type="unfinished">Sazlamalar</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="213"/>
        <source>Translate The App...</source>
        <translation type="unfinished">Programmany terjime et ...</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="220"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="228"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="241"/>
        <source>Create Default Library</source>
        <translation type="unfinished">Bellenen kitaphanany dörediň</translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="353"/>
        <source>Hide Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="353"/>
        <source>Show Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="364"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="373"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/LibrariesSideBar.qml" line="386"/>
        <source>Schedule</source>
        <translation type="unfinished">Iş tertibi</translation>
    </message>
</context>
<context>
    <name>LibraryPage</name>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="161"/>
        <source>Red</source>
        <translation type="unfinished">Gyzyl</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="168"/>
        <source>Green</source>
        <translation type="unfinished">Greenaşyl</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="175"/>
        <source>Blue</source>
        <translation type="unfinished">Gök</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="182"/>
        <source>Yellow</source>
        <translation type="unfinished">Sary</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="189"/>
        <source>Orange</source>
        <translation type="unfinished">Mämişi</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="196"/>
        <source>Lilac</source>
        <translation type="unfinished">Kirpik</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="203"/>
        <source>White</source>
        <translation type="unfinished">Ak</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="213"/>
        <source>Rename</source>
        <translation type="unfinished">Adyny üýtgetmek</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="218"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="223"/>
        <source>Delete</source>
        <translation type="unfinished">Öçür</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="232"/>
        <source>Select Image</source>
        <translation type="unfinished">Surat saýlaň</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="291"/>
        <source>Note Title</source>
        <translation type="unfinished">Bellik ady</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="304"/>
        <source>Todo List Title</source>
        <translation type="unfinished">Todo sanawyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="317"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="396"/>
        <source>Nothing here yet! Start by adding a &lt;a href=&apos;#note&apos;&gt;note&lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt;todo list&lt;/a&gt; or &lt;a href=&apos;#image&apos;&gt;image&lt;/a&gt;.</source>
        <translation type="unfinished">Bu ýerde entek hiç zat ýok! &lt;a href=&apos;#note&apos;&gt; bellik &lt;/a&gt;, &lt;a href=&apos;#todolist&apos;&gt; todo sanawy &lt;/a&gt; ýa-da &lt;a href=&apos;#image&apos;&gt; surat &lt;/a&gt; goşmak bilen başlaň.</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="456"/>
        <source>Sort By</source>
        <translation type="unfinished">Sort boýunça</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="461"/>
        <source>Manually</source>
        <translation type="unfinished">El bilen</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="468"/>
        <source>Title</source>
        <translation type="unfinished">Ady</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="475"/>
        <source>Due To</source>
        <translation type="unfinished">Sebäbi</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="482"/>
        <source>Created At</source>
        <translation type="unfinished">Döredildi</translation>
    </message>
    <message>
        <location filename="../Pages/LibraryPage.qml" line="489"/>
        <source>Updated At</source>
        <translation type="unfinished">Täzelendi</translation>
    </message>
</context>
<context>
    <name>LibraryPageActions</name>
    <message>
        <location filename="../Menues/LibraryPageActions.qml" line="30"/>
        <source>Sync Now</source>
        <translation type="unfinished">Indi sinhronlaň</translation>
    </message>
    <message>
        <location filename="../Menues/LibraryPageActions.qml" line="41"/>
        <source>Sync Log</source>
        <translation type="unfinished">Sinhron ýazgysy</translation>
    </message>
</context>
<context>
    <name>LogViewPage</name>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="14"/>
        <source>Synchronization Log</source>
        <translation type="unfinished">Sinhronizasiýa gündeligi</translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="26"/>
        <source>Copy Log</source>
        <translation type="unfinished">Logurnaldan göçüriň</translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="32"/>
        <source>Scroll to Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/LogViewPage.qml" line="38"/>
        <source>Scroll to Bottom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Windows/MainWindow.qml" line="24"/>
        <source>OpenTodoList</source>
        <translation type="unfinished">OpenTodoList</translation>
    </message>
    <message>
        <location filename="../Windows/MainWindow.qml" line="276"/>
        <source>Start by &lt;a href=&apos;#newLibrary&apos;&gt;creating a new library&lt;/a&gt;. Libraries are used to store different kinds of items like notes, todo lists and images.</source>
        <translation type="unfinished">&lt;a href=&apos;#newLibrary&apos;&gt; täze kitaphana döretmekden başlaň &lt;/a&gt;. Kitaphanalar bellikler, sanawlar we suratlar ýaly dürli zatlary saklamak üçin ulanylýar.</translation>
    </message>
</context>
<context>
    <name>MoveTodo</name>
    <message>
        <location filename="../Actions/MoveTodo.qml" line="11"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewDropboxAccountPage</name>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="20"/>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="54"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="62"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="73"/>
        <source>We have tried to open your browser to log you in to your Dropbox account. Please log in and grant access to OpenTodoList in order to proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="80"/>
        <source>Didn&apos;t your browser open? You can retry opening it or copy the required URL manually to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="86"/>
        <source>Authorize...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="95"/>
        <source>Open Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="101"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="105"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="114"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/NewDropboxAccountPage.qml" line="121"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewItemWithDueDateDialog</name>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="92"/>
        <source>Today</source>
        <translation type="unfinished">Bu gün</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="93"/>
        <source>Tomorrow</source>
        <translation type="unfinished">Ertir</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="94"/>
        <source>This Week</source>
        <translation type="unfinished">Bu hepde</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="95"/>
        <source>Next Week</source>
        <translation type="unfinished">Indiki hepde</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="96"/>
        <source>Select...</source>
        <translation type="unfinished">Saýlaň ...</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="115"/>
        <source>Title:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="120"/>
        <source>The title for your new item...</source>
        <translation type="unfinished">Täze elementiňiziň ady ...</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="126"/>
        <source>Create in:</source>
        <translation type="unfinished">Dörediň:</translation>
    </message>
    <message>
        <location filename="../Windows/NewItemWithDueDateDialog.qml" line="166"/>
        <source>Due on:</source>
        <translation type="unfinished">Berilmeli:</translation>
    </message>
</context>
<context>
    <name>NewLibraryFromAccountPage</name>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="82"/>
        <source>Create Library in Account</source>
        <translation type="unfinished">Hasapda kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="91"/>
        <source>A library created in an account is synchronized with it. This allows to easily back up a library to a server and later on restore it from there. Additionally, such libraries can be shared with other users (if the server allows this).</source>
        <translation type="unfinished">Hasapda döredilen kitaphana onuň bilen sinhronlaşdyrylýar. Bu kitaphanany serwere aňsatlyk bilen ätiýaçlamaga we soňra şol ýerden dikeltmäge mümkinçilik berýär. Mundan başga-da, şeýle kitaphanalary beýleki ulanyjylar bilen paýlaşyp bolýar (serwer muňa rugsat berse).</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="99"/>
        <source>Existing Libraries</source>
        <translation type="unfinished">Bar bolan kitaphanalar</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="107"/>
        <source>Select an existing library on the server to add it to the app.</source>
        <translation type="unfinished">Programma goşmak üçin serwerde bar bolan kitaphanany saýlaň.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="113"/>
        <source>No libraries were found on the server.</source>
        <translation type="unfinished">Serwerde kitaphanalar tapylmady.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="122"/>
        <source>Searching existing libraries...</source>
        <translation type="unfinished">Bar bolan kitaphanalary gözlemek ...</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="165"/>
        <source>Create a New Library</source>
        <translation type="unfinished">Täze kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="173"/>
        <source>Create a new library, which will be synchronized with the server. Such a library can be added to the app on other devices as well to synchronize data.</source>
        <translation type="unfinished">Serwer bilen sinhronlanjak täze kitaphana dörediň. Şeýle kitaphanany beýleki enjamlardaky programma goşmak bilen maglumatlary sinhronlamak üçin hem goşup bolýar.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryFromAccountPage.qml" line="187"/>
        <source>My new library&apos;s name</source>
        <translation type="unfinished">Täze kitaphanamyň ady</translation>
    </message>
</context>
<context>
    <name>NewLibraryInFolderPage</name>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="74"/>
        <source>Select a Folder</source>
        <translation type="unfinished">Papka saýlaň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="101"/>
        <source>Open a Folder as a Library</source>
        <translation type="unfinished">Kitaphana hökmünde bukjany açyň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="109"/>
        <source>You can use any folder as a location for a library.&lt;br/&gt;&lt;br/&gt;This is especially useful when you want to use another tool (like a sync client of a cloud provider) to sync your data with a server.</source>
        <translation type="unfinished">Islendik bukjany kitaphananyň ýerleşýän ýeri hökmünde ulanyp bilersiňiz. &lt;br/&gt; &lt;br/&gt; Maglumatlaryňyzy serwer bilen sinhronlamak üçin başga bir gural (bulut üpjünçisiniň sinhron müşderisi ýaly) ulanmak isleseňiz has peýdalydyr.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="113"/>
        <source>Folder:</source>
        <translation type="unfinished">Papka:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="118"/>
        <source>Path to a folder to use as a library</source>
        <translation type="unfinished">Kitaphana hökmünde ulanmak üçin bukja barýan ýol</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="124"/>
        <source>Select</source>
        <translation type="unfinished">Saýlaň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="129"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryInFolderPage.qml" line="134"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Localerli kitaphanamyň ady</translation>
    </message>
</context>
<context>
    <name>NewLibraryPage</name>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="23"/>
        <location filename="../Pages/NewLibraryPage.qml" line="63"/>
        <source>Create Library</source>
        <translation type="unfinished">Kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="70"/>
        <source>Local Library</source>
        <translation type="unfinished">Libraryerli kitaphana</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="77"/>
        <source>Use Folder as Library</source>
        <translation type="unfinished">Papkany kitaphana hökmünde ulanyň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="89"/>
        <source>Add Libraries From Your Accounts</source>
        <translation type="unfinished">Hasabyňyzdan kitaphanalary goşuň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLibraryPage.qml" line="116"/>
        <source>Add Account</source>
        <translation type="unfinished">Hasap goşuň</translation>
    </message>
</context>
<context>
    <name>NewLocalLibraryPage</name>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="62"/>
        <source>Create a Local Library</source>
        <translation type="unfinished">Libraryerli kitaphana dörediň</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="70"/>
        <source>A local library is stored solely on your device - this makes it perfect for the privacy concise!&lt;br/&gt;&lt;br/&gt;Use it when you want to store information only locally and back up all your data regularly via other mechanisms. If you need to access your information across several devices, create a library which is synced instead.</source>
        <translation type="unfinished">Libraryerli kitaphana diňe enjamyňyzda saklanýar - bu gizlinlik gysga bolmagy üçin ajaýyp edýär! &lt;br/&gt; &lt;br/&gt; Maglumatlary diňe ýerli derejede saklamak we ähli maglumatlaryňyzy beýleki mehanizmler arkaly yzygiderli ätiýaçlandyrmak isleseňiz ulanyň. Maglumatlaryňyza birnäçe enjamda girmek zerur bolsa, ýerine sinhronlanan kitaphanany dörediň.</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="80"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/NewLocalLibraryPage.qml" line="85"/>
        <source>My Local Library Name</source>
        <translation type="unfinished">Localerli kitaphanamyň ady</translation>
    </message>
</context>
<context>
    <name>NewNextCloudAccountPage</name>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="20"/>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="73"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="79"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="93"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="109"/>
        <source>Trouble Signing In?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="120"/>
        <source>We have tried to open your browser to log you in to your NextCloud instance. Please log in and grant access to OpenTodoList in order to proceed. Trouble accessing your NextCloud in the browser? You can manually enter your username and password as well.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="128"/>
        <source>Log in Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="134"/>
        <source>Ideally, you use app specific passwords instead of your user password. In case your login is protected with 2 Factor Authentication (2FA) you even must use app specific passwords to access your NextCloud. You can create such passwords in your user settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="143"/>
        <source>Create App Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="152"/>
        <source>Account Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="162"/>
        <source>Copy Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="168"/>
        <source>Copied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="177"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="184"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="191"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="198"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="208"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="212"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="225"/>
        <source>Account Name</source>
        <translation type="unfinished">Hasabyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/NewNextCloudAccountPage.qml" line="235"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>NewTopLevelItemButton</name>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="43"/>
        <source>Todo List</source>
        <translation type="unfinished">Todo sanawy</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="48"/>
        <source>Todo</source>
        <translation type="unfinished">Todo</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="54"/>
        <source>Note</source>
        <translation type="unfinished">Bellik</translation>
    </message>
    <message>
        <location filename="../Widgets/NewTopLevelItemButton.qml" line="58"/>
        <source>Image</source>
        <translation type="unfinished">Surat</translation>
    </message>
</context>
<context>
    <name>NewWebDAVAccountPage</name>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="29"/>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="101"/>
        <source>Connection Settings</source>
        <translation type="unfinished">Birikdiriş sazlamalary</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="107"/>
        <source>Server Address:</source>
        <translation type="unfinished">Serweriň salgysy:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="113"/>
        <source>https://myserver.example.com</source>
        <translation type="unfinished">https://myserver.example.com</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="119"/>
        <source>User:</source>
        <translation type="unfinished">Ulanyjy:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="125"/>
        <source>User Name</source>
        <translation type="unfinished">Ulanyjynyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="131"/>
        <source>Password:</source>
        <translation type="unfinished">Parol:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="137"/>
        <source>Password</source>
        <translation type="unfinished">Parol</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="146"/>
        <source>Disable Certificate Checks</source>
        <translation type="unfinished">Şahadatnamalary barlaň</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="150"/>
        <source>Name:</source>
        <translation type="unfinished">Ady:</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="163"/>
        <source>Account Name</source>
        <translation type="unfinished">Hasabyň ady</translation>
    </message>
    <message>
        <location filename="../Pages/NewWebDAVAccountPage.qml" line="173"/>
        <source>Failed to connect to the server. Please check your user name, password and the server address and retry.</source>
        <translation type="unfinished">Serwere birikdirilmedi. Ulanyjy adyňyzy, parolyňyzy we serwer salgyňyzy barlaň we gaýtadan synanyşyň.</translation>
    </message>
</context>
<context>
    <name>NoteItem</name>
    <message>
        <location filename="../Widgets/NoteItem.qml" line="75"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotePage</name>
    <message>
        <location filename="../Pages/NotePage.qml" line="132"/>
        <source>Main Page</source>
        <translation type="unfinished">Baş sahypa</translation>
    </message>
    <message>
        <location filename="../Pages/NotePage.qml" line="195"/>
        <source>New Page</source>
        <translation type="unfinished">Täze sahypa</translation>
    </message>
</context>
<context>
    <name>OpenTodoList::Translations</name>
    <message>
        <location filename="../../lib/utils/translations.cpp" line="91"/>
        <source>System Language</source>
        <translation type="unfinished">Ulgam dili</translation>
    </message>
</context>
<context>
    <name>ProblemsPage</name>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="18"/>
        <location filename="../Pages/ProblemsPage.qml" line="28"/>
        <source>Problems Detected</source>
        <translation type="unfinished">Meseleler tapyldy</translation>
    </message>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="42"/>
        <source>Missing secrets for account</source>
        <translation type="unfinished">Hasap üçin giriş maglumatlary ýok</translation>
    </message>
    <message>
        <location filename="../Pages/ProblemsPage.qml" line="47"/>
        <source>Synchronization failed for library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PromoteTask</name>
    <message>
        <location filename="../Actions/PromoteTask.qml" line="12"/>
        <source>Promote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appstartup.cpp" line="280"/>
        <location filename="../appstartup.cpp" line="287"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuickNoteWindow</name>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="19"/>
        <source>Quick Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="46"/>
        <source>Quick Notes Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="57"/>
        <source>Open the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="70"/>
        <source>Quick Note Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="83"/>
        <source>Type your notes here...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="133"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="140"/>
        <source>Save the entered notes to the selected library. Press and hold the button to get more options for saving.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="158"/>
        <source>Save as Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="161"/>
        <source>Quick Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="169"/>
        <source>Save as Todo List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/QuickNoteWindow.qml" line="171"/>
        <source>Quick Todo List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecurrenceDialog</name>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="30"/>
        <source>Edit Recurrence</source>
        <translation type="unfinished">Urrygy-ýygydan redaktirläň</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="43"/>
        <source>Never</source>
        <translation type="unfinished">Hiç haçan</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="44"/>
        <source>Daily</source>
        <translation type="unfinished">Her gün</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="45"/>
        <source>Weekly</source>
        <translation type="unfinished">Hepdelik</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="46"/>
        <source>Monthly</source>
        <translation type="unfinished">Aýlyk</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="47"/>
        <source>Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="48"/>
        <source>Every N Days</source>
        <translation type="unfinished">Her N gün</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="49"/>
        <source>Every N Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="50"/>
        <source>Every N Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="75"/>
        <source>Recurs:</source>
        <translation type="unfinished">Gaýtalama:</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="88"/>
        <source>Number of days:</source>
        <translation type="unfinished">Günleriň sany:</translation>
    </message>
    <message>
        <location filename="../Windows/RecurrenceDialog.qml" line="106"/>
        <source>Recur relative to the date when marking as done</source>
        <translation type="unfinished">Edilişi ýaly bellik edilende senä görä gaýtalaň</translation>
    </message>
</context>
<context>
    <name>RenameItem</name>
    <message>
        <location filename="../Actions/RenameItem.qml" line="8"/>
        <source>Rename</source>
        <translation type="unfinished">Adyny üýtgetmek</translation>
    </message>
</context>
<context>
    <name>RenameItemDialog</name>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="22"/>
        <source>Rename Item</source>
        <translation type="unfinished">Harydyň adyny üýtgediň</translation>
    </message>
    <message>
        <location filename="../Windows/RenameItemDialog.qml" line="33"/>
        <source>Enter item title...</source>
        <translation type="unfinished">Harydyň adyny giriziň ...</translation>
    </message>
</context>
<context>
    <name>RenameLibraryDialog</name>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="20"/>
        <source>Rename Library</source>
        <translation type="unfinished">Kitaphananyň adyny üýtgediň</translation>
    </message>
    <message>
        <location filename="../Windows/RenameLibraryDialog.qml" line="37"/>
        <source>Enter library title...</source>
        <translation type="unfinished">Kitaphananyň adyny giriziň ...</translation>
    </message>
</context>
<context>
    <name>ResetDueTo</name>
    <message>
        <location filename="../Actions/ResetDueTo.qml" line="8"/>
        <source>Reset Due To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScheduleViewPage</name>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="101"/>
        <source>Overdue</source>
        <translation type="unfinished">Wagtyň geçmegi</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="123"/>
        <source>Today</source>
        <translation type="unfinished">Bu gün</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="124"/>
        <source>Tomorrow</source>
        <translation type="unfinished">Ertir</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="166"/>
        <source>Later This Week</source>
        <translation type="unfinished">Bu hepde soň</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="168"/>
        <source>Next Week</source>
        <translation type="unfinished">Indiki hepde</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="169"/>
        <source>Coming Next</source>
        <translation type="unfinished">Indiki</translation>
    </message>
    <message>
        <location filename="../Pages/ScheduleViewPage.qml" line="196"/>
        <source>Nothing scheduled... Add a due date to items for them to appear here.</source>
        <translation type="unfinished">Hiç zat meýilleşdirilmedi ... Bu ýerde peýda bolmagy üçin elementlere bellenilen senäni goşuň.</translation>
    </message>
</context>
<context>
    <name>SelectLibraryDialog</name>
    <message>
        <location filename="../Windows/SelectLibraryDialog.qml" line="22"/>
        <source>Select Library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTodoListDialog</name>
    <message>
        <location filename="../Windows/SelectTodoListDialog.qml" line="25"/>
        <source>Select Todo List</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectTopLevelItemDialog</name>
    <message>
        <location filename="../Windows/SelectTopLevelItemDialog.qml" line="22"/>
        <source>Select Item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetDueNextWeek</name>
    <message>
        <location filename="../Actions/SetDueNextWeek.qml" line="8"/>
        <source>Set Due This Week</source>
        <translation type="unfinished">Şu hepde bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueThisWeek</name>
    <message>
        <location filename="../Actions/SetDueThisWeek.qml" line="8"/>
        <source>Set Due Next Week</source>
        <translation type="unfinished">Geljek hepde bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueTo</name>
    <message>
        <location filename="../Actions/SetDueTo.qml" line="8"/>
        <source>Select Due Date</source>
        <translation type="unfinished">Berlen senäni saýlaň</translation>
    </message>
</context>
<context>
    <name>SetDueToday</name>
    <message>
        <location filename="../Actions/SetDueToday.qml" line="8"/>
        <source>Set Due Today</source>
        <translation type="unfinished">Şu gün bellemeli</translation>
    </message>
</context>
<context>
    <name>SetDueTomorrow</name>
    <message>
        <location filename="../Actions/SetDueTomorrow.qml" line="8"/>
        <source>Set Due Tomorrow</source>
        <translation type="unfinished">Ertir bermeli</translation>
    </message>
</context>
<context>
    <name>SetManualProgressAction</name>
    <message>
        <location filename="../Actions/SetManualProgressAction.qml" line="8"/>
        <source>Set Progress</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="45"/>
        <source>Settings</source>
        <translation type="unfinished">Sazlamalar</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="51"/>
        <source>User Interface</source>
        <translation type="unfinished">Ulanyjy interfeýsi</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="59"/>
        <source>Language:</source>
        <translation type="unfinished">Dil:</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="105"/>
        <source>Theme:</source>
        <translation type="unfinished">Mowzuk:</translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="129"/>
        <source>System Tray:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="135"/>
        <source>Open Quick Notes Editor on Click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="141"/>
        <source>Font Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="158"/>
        <source>Use custom font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="167"/>
        <source>Desktop Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="176"/>
        <source>Use Compact Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="185"/>
        <source>Reduce space between components and reduce the font size.

&lt;em&gt;Requires a restart of the app.&lt;/em&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="193"/>
        <source>Use compact todo lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="203"/>
        <source>Reduce the padding in todo and task listings to fit more items on the screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="210"/>
        <source>Override Scaling Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="217"/>
        <source>Scale Factor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="235"/>
        <source>Use this to manually scale the user interface. By default, the app should adapt automatically according to your device configuration. If this does not work properly, you can set a custom scaling factor here.

This requires a restart of the app.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/SettingsPage.qml" line="244"/>
        <source>Library Item Size:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SyncErrorNotificationBar</name>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="43"/>
        <source>There were errors when synchronizing the library. Please ensure that the library settings are up to date.</source>
        <translation type="unfinished">Kitaphanany sinhronlamakda ýalňyşlyklar boldy. Kitaphana sazlamalarynyň täzelenendigine göz ýetiriň.</translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="49"/>
        <source>Ignore</source>
        <translation type="unfinished">Üns berme</translation>
    </message>
    <message>
        <location filename="../Widgets/SyncErrorNotificationBar.qml" line="53"/>
        <source>View</source>
        <translation type="unfinished">Gör</translation>
    </message>
</context>
<context>
    <name>TagsEditor</name>
    <message>
        <location filename="../Widgets/TagsEditor.qml" line="34"/>
        <source>Add Tag</source>
        <translation type="unfinished">Bellik goşuň</translation>
    </message>
</context>
<context>
    <name>TodoListItem</name>
    <message>
        <location filename="../Widgets/TodoListItem.qml" line="76"/>
        <source>Due on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Widgets/TodoListItem.qml" line="128"/>
        <source>✔ No open todos - everything done</source>
        <translation type="unfinished">Open Açyk todos ýok - hemme zat ýerine ýetirildi</translation>
    </message>
</context>
<context>
    <name>TodoListPage</name>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="160"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="172"/>
        <source>Manually</source>
        <translation type="unfinished">El bilen</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="178"/>
        <source>Name</source>
        <translation type="unfinished">Ady</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="184"/>
        <source>Due Date</source>
        <translation type="unfinished">Berlen senesi</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="190"/>
        <source>Created At</source>
        <translation type="unfinished">Döredildi</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="196"/>
        <source>Updated At</source>
        <translation type="unfinished">Täzelendi</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="209"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="216"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="246"/>
        <source>Todos</source>
        <translation type="unfinished">Todos</translation>
    </message>
    <message>
        <location filename="../Pages/TodoListPage.qml" line="266"/>
        <source>Add new todo...</source>
        <translation type="unfinished">Täze todo goşuň ...</translation>
    </message>
</context>
<context>
    <name>TodoPage</name>
    <message>
        <location filename="../Pages/TodoPage.qml" line="147"/>
        <source>Search term 1, search term 2, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="172"/>
        <source>Tasks</source>
        <translation type="unfinished">Wezipeler</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="178"/>
        <source>Add new task...</source>
        <translation type="unfinished">Täze wezipe goşuň ...</translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="274"/>
        <source>Show Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Pages/TodoPage.qml" line="281"/>
        <source>Show At The End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TodosWidget</name>
    <message>
        <location filename="../Widgets/TodosWidget.qml" line="203"/>
        <source>Due on: %1</source>
        <translation type="unfinished">Berilmeli: %1</translation>
    </message>
</context>
<context>
    <name>TodosWidgetDelegate</name>
    <message>
        <location filename="../Widgets/TodosWidgetDelegate.qml" line="276"/>
        <source>Swipe to mark undone</source>
        <translation type="unfinished">Yzyna gaýtarylmagyny belläň</translation>
    </message>
    <message>
        <location filename="../Widgets/TodosWidgetDelegate.qml" line="278"/>
        <source>Swipe to mark done</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateNotificationBar</name>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="44"/>
        <source>An update to OpenTodoList %1 is available.</source>
        <translation type="unfinished">OpenTodoList %1 täzelenmesi bar.</translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="50"/>
        <source>Ignore</source>
        <translation type="unfinished">Üns berme</translation>
    </message>
    <message>
        <location filename="../Widgets/UpdateNotificationBar.qml" line="54"/>
        <source>Download</source>
        <translation type="unfinished">Göçürip al</translation>
    </message>
</context>
</TS>
